dmcp_cargs = [
  '-D__weak="__attribute__((weak))"',
  '-D__packed="__attribute__((__packed__))"',
  '-DOLD_HW',
  '-Wno-unused-parameter']
dmcp_linkargs = [
  '--specs=nosys.specs',
  '-Wl,--gc-sections',
  '-Wl,--wrap=_malloc_r',
  '-Wl,-Map=' + meson.current_build_dir() + '/C47.map,--cref',
  '-T' + meson.current_source_dir() + '/stm32_program.ld']

c47_dmcp_src = files(
  'hal/audio.c',
  'hal/io.c')
c47_lib = static_library(
  'libc47',
  build_by_default    : false,
  sources             : decNumber_src + c47_src + c47_dmcp_src +
                        [rasterFontsData_c, softmenuCatalogs_h,
                          constantPointers_ch, vcs_h, version_h],
  c_args              : dmcp_cargs,
  include_directories : [decNumber_inc, c47_inc, dmcp_inc],
  dependencies        : arm_gmp_dep,
  pic                 : false,
  override_options    : ['debug=false', 'optimization=s'])

# First pass building the ELF to get the size of the QSPI
c47_pre_elf = executable(
  'C47_pre.elf',
  build_by_default    : false,
  sources             : dmcp_src + [vcs_h, version_h],
  c_args              : dmcp_cargs,
  link_args           : dmcp_linkargs,
  link_with           : c47_lib,
  include_directories : [c47_inc, dmcp_inc],
  pie                 : false,
  override_options    : ['debug=false', 'optimization=s'])
c47_pre_qspi_incorrect_crc = custom_target(
  'C47_pre_qspi_incorrect_crc.bin',
  build_by_default : false,
  input            : [c47_pre_elf],
  output           : 'C47_pre_qspi_incorrect_crc.bin',
  command          : [arm_objcopy, '--only-section', '.qspi', '-O', 'binary', '@INPUT@', '@OUTPUT@'])
c47_pre_qspi = custom_target(
  'C47_pre_qspi.bin',
  build_by_default : false,
  input            : [c47_pre_qspi_incorrect_crc],
  output           : 'C47_pre_qspi.bin',
  command          : [bash, modify_crc, forcecrc32, '@INPUT@', '@OUTPUT@'])
generated_qspi_crc_h = custom_target(
  'generated_qspi_crc.h',
  build_by_default : false,
  input            : [c47_pre_qspi],
  output           : 'generated_qspi_crc.h',
  command          : [bash, gen_qspi_crc, '@INPUT@', '@OUTPUT@'])

# Second pass building the ELF including the correct qspi_crc.h
c47_elf = executable(
  'C47.elf',
  build_by_default    : false,
  sources             : dmcp_src + [vcs_h, version_h, generated_qspi_crc_h],
  c_args              : dmcp_cargs + ['-DUSE_GEN_QSPI_CRC'],
  link_args           : dmcp_linkargs,
  link_with           : c47_lib,
  include_directories : [c47_inc, dmcp_inc],
  pie                 : false,
  override_options    : ['debug=false', 'optimization=s'])
c47_flash = custom_target(
  'C47_flash.bin',
  build_by_default : false,
  input            : [c47_elf],
  output           : 'C47_flash.bin',
  command          : [arm_objcopy, '--remove-section', '.qspi', '-O', 'binary', '@INPUT@', '@OUTPUT@'])
c47_pgm = custom_target(
  'C47.pgm',
  build_by_default : false,
  input            : [c47_flash],
  output           : 'C47.pgm',
  command          : [bash, add_pgm_chsum, '@INPUT@', '@OUTPUT@'])
c47_qspi_incorrect_crc = custom_target(
  'C47_qspi_incorrect_crc.bin',
  build_by_default : false,
  input            : [c47_elf],
  output           : 'C47_qspi_incorrect_crc.bin',
  command          : [arm_objcopy, '--only-section', '.qspi', '-O', 'binary', '@INPUT@', '@OUTPUT@'])
c47_qspi = custom_target(
  'C47_qspi.bin',
  build_by_default : false,
  input            : [c47_qspi_incorrect_crc],
  output           : 'C47_qspi.bin',
  command          : [bash, modify_crc, forcecrc32, '@INPUT@', '@OUTPUT@'])

alias_target('dmcp', c47_pgm, c47_qspi)
