// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 and C47 Authors

#include "store.h"

#include "charString.h"
#include "debug.h"
#include "defines.h"
#include "error.h"
#include "flags.h"
#include "items.h"
#include "c43Extensions/jm.h"
#include "mathematics/compare.h"
#include "mathematics/integerPart.h"
#include "mathematics/matrix.h"
#include "plotstat.h"
#include "registerValueConversions.h"
#include "registers.h"
#include "stack.h"
#include "stats.h"
#include "typeDefinitions.h"
#include "ui/matrixEditor.h"

#include "c47.h"



bool_t regInRange(uint16_t regist) {
  bool_t inRange = (
    (regist < FIRST_LOCAL_REGISTER + currentNumberOfLocalRegisters) ||
    (FIRST_NAMED_VARIABLE <= regist && regist < FIRST_NAMED_VARIABLE + numberOfNamedVariables) ||
    (FIRST_RESERVED_VARIABLE <= regist && regist <= LAST_RESERVED_VARIABLE));
  #if defined(PC_BUILD)
    if(!inRange) {
      if(regist >= FIRST_LOCAL_REGISTER && regist <= LAST_LOCAL_REGISTER) {
        displayCalcErrorMessage(ERROR_OUT_OF_RANGE, ERR_REGISTER_LINE, REGISTER_X);
        sprintf(errorMessage, "local register .%02d", regist - FIRST_LOCAL_REGISTER);
      }
      else if(regist >= FIRST_NAMED_VARIABLE && regist <= LAST_NAMED_VARIABLE) {
        displayCalcErrorMessage(ERROR_UNDEF_SOURCE_VAR, ERR_REGISTER_LINE, REGISTER_X);
        // This error message is not massively useful because it doesn't have the original name
        // But it shouldn't have even got this far if the name doesn't exist
        sprintf(errorMessage, "named register .%02d", regist - FIRST_NAMED_VARIABLE);
      }
      else {
        displayCalcErrorMessage(ERROR_OUT_OF_RANGE, ERR_REGISTER_LINE, REGISTER_X);
      }
      moreInfoOnError("In function regInRange:", errorMessage, " is not defined!", NULL);
    }
  #endif // PC_BUILD
  return inRange;
}

static bool_t _checkReadOnlyVariable(uint16_t regist) {
  if(FIRST_RESERVED_VARIABLE <= regist && regist <= LAST_RESERVED_VARIABLE && allReservedVariables[regist - FIRST_RESERVED_VARIABLE].header.readOnly == 1) {
    displayCalcErrorMessage(ERROR_WRITE_PROTECTED_VAR, ERR_REGISTER_LINE, REGISTER_X);
    #if(EXTRA_INFO_ON_CALC_ERROR == 1)
      sprintf(errorMessage, "reserved variable %s", allReservedVariables[regist - FIRST_RESERVED_VARIABLE].reservedVariableName + 1);
      moreInfoOnError("In function _checkReadOnlyVariable:", errorMessage, " is write-protected!", NULL);
    #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
    return false;
  }
  else {
    return true;
  }
}



#if !defined(TESTSUITE_BUILD)
  static bool_t storeElementReal(real34Matrix_t *matrix) {
    const int16_t i = getIRegisterAsInt(true);
    const int16_t j = getJRegisterAsInt(true);

    if(getRegisterDataType(REGISTER_X) == dtLongInteger) {
      convertLongIntegerRegisterToReal34(REGISTER_X, &matrix->matrixElements[i * matrix->header.matrixColumns + j]);
    }
    else if(getRegisterDataType(REGISTER_X) == dtReal34) {
      real34Copy(REGISTER_REAL34_DATA(REGISTER_X), &matrix->matrixElements[i * matrix->header.matrixColumns + j]);
    }
    else {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      #if(EXTRA_INFO_ON_CALC_ERROR == 1)
        sprintf(errorMessage, "Cannot store %s in a matrix", getRegisterDataTypeName(REGISTER_X, true, false));
        moreInfoOnError("In function storeElementReal:", errorMessage, NULL, NULL);
      #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
      return false;
    }
    return true;
  }

  static bool_t storeElementComplex(complex34Matrix_t *matrix) {
    const int16_t i = getIRegisterAsInt(true);
    const int16_t j = getJRegisterAsInt(true);

    if(getRegisterDataType(REGISTER_X) == dtLongInteger) {
      convertLongIntegerRegisterToReal34(REGISTER_X, VARIABLE_REAL34_DATA(&matrix->matrixElements[i * matrix->header.matrixColumns + j]));
      real34Zero(VARIABLE_IMAG34_DATA(&matrix->matrixElements[i * matrix->header.matrixColumns + j]));
    }
    else if(getRegisterDataType(REGISTER_X) == dtReal34) {
      real34Copy(REGISTER_REAL34_DATA(REGISTER_X), VARIABLE_REAL34_DATA(&matrix->matrixElements[i * matrix->header.matrixColumns + j]));
      real34Zero(VARIABLE_IMAG34_DATA(&matrix->matrixElements[i * matrix->header.matrixColumns + j]));
    }
    else if(getRegisterDataType(REGISTER_X) == dtComplex34) {
      real34Copy(REGISTER_REAL34_DATA(REGISTER_X), VARIABLE_REAL34_DATA(&matrix->matrixElements[i * matrix->header.matrixColumns + j]));
      real34Copy(REGISTER_IMAG34_DATA(REGISTER_X), VARIABLE_IMAG34_DATA(&matrix->matrixElements[i * matrix->header.matrixColumns + j]));
    }
    else {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      #if(EXTRA_INFO_ON_CALC_ERROR == 1)
        sprintf(errorMessage, "Cannot store %s in a matrix", getRegisterDataTypeName(REGISTER_X, true, false));
        moreInfoOnError("In function storeElementReal:", errorMessage, NULL, NULL);
      #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
      return false;
    }
    return true;
  }



  static bool_t storeIjReal(real34Matrix_t *matrix) {
    if(getRegisterDataType(REGISTER_X) == dtLongInteger && getRegisterDataType(REGISTER_Y) == dtLongInteger) {
      longInteger_t i, j;
      convertLongIntegerRegisterToLongInteger(REGISTER_Y, i);
      convertLongIntegerRegisterToLongInteger(REGISTER_X, j);
      if(longIntegerCompareInt(i, 0) > 0 && longIntegerCompareUInt(i, matrix->header.matrixRows) <= 0 && longIntegerCompareInt(j, 0) > 0 && longIntegerCompareUInt(j, matrix->header.matrixColumns) <= 0) {
        copySourceRegisterToDestRegister(REGISTER_Y, REGISTER_I);
        copySourceRegisterToDestRegister(REGISTER_X, REGISTER_J);
      }
      else {
        #if(EXTRA_INFO_ON_CALC_ERROR == 1)
          uint16_t row, col;
          longIntegerToUInt(i, row);
          longIntegerToUInt(j, col);
        #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
        displayCalcErrorMessage(ERROR_OUT_OF_RANGE, ERR_REGISTER_LINE, REGISTER_X);
        #if(EXTRA_INFO_ON_CALC_ERROR == 1)
          sprintf(errorMessage, "(%" PRIu16 ", %" PRIu16 ") out of range", row, col);
          moreInfoOnError("In function storeIJReal:", errorMessage, NULL, NULL);
        #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
      }
      longIntegerFree(i);
      longIntegerFree(j);
    }
    else {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      #if(EXTRA_INFO_ON_CALC_ERROR == 1)
        sprintf(errorMessage, "Cannot store %s in a matrix", getRegisterDataTypeName(REGISTER_X, true, false));
        moreInfoOnError("In function storeIJReal:", errorMessage, NULL, NULL);
      #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
    }
    return false;
  }

  static bool_t storeIjComplex(complex34Matrix_t *matrix) {
    return storeIjReal((real34Matrix_t *)matrix);
  }
#endif // !TESTSUITE_BUILD



static void _storeValue(uint16_t regist) {
  if(regist == RESERVED_VARIABLE_GRAMOD) {
    copySourceRegisterToDestRegister(REGISTER_X, TEMP_REGISTER_1);
    fnIp(NOPARAM);
    if(lastErrorCode == ERROR_NONE) {
      longInteger_t x;
      convertLongIntegerRegisterToLongInteger(REGISTER_X, x);
      if(longIntegerCompareInt(x, 0) >= 0 && longIntegerCompareInt(x, 3) <= 0) {
        copySourceRegisterToDestRegister(REGISTER_X, regist);
        copySourceRegisterToDestRegister(TEMP_REGISTER_1, REGISTER_X);
      }
      else {
        displayCalcErrorMessage(ERROR_OUT_OF_RANGE, ERR_REGISTER_LINE, REGISTER_X);
        #if(EXTRA_INFO_ON_CALC_ERROR == 1)
          moreInfoOnError("In function _storeValue:", "Invalid value for GRAMOD", NULL, NULL);
        #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
      }
      longIntegerFree(x);
    }
  }
  else if(FIRST_RESERVED_VARIABLE <= regist && regist <= LAST_RESERVED_VARIABLE && allReservedVariables[regist - FIRST_RESERVED_VARIABLE].header.dataType == dtReal34) {
    copySourceRegisterToDestRegister(REGISTER_X, TEMP_REGISTER_1);
    fnToReal(NOPARAM);
    if(lastErrorCode == ERROR_NONE) {
      copySourceRegisterToDestRegister(REGISTER_X, regist);
      copySourceRegisterToDestRegister(TEMP_REGISTER_1, REGISTER_X);
    }
  }
  else {
    copySourceRegisterToDestRegister(REGISTER_X, regist);
  }
}

void fnStore(uint16_t regist) {
  if(_checkReadOnlyVariable(regist) && regInRange(regist)) {
    _storeValue(regist);
    uint16_t rows = 1;
    if(regist >= FIRST_NAMED_VARIABLE && isStatsMatrixN(&rows, regist) && regist == findNamedVariable("STATS")) {
      calcSigma(0);
    }
    if(regist == REGISTER_I || regist == REGISTER_J) {
      temporaryInformation = TI_IJ;
    }
  }
}



void fnStoreAdd(uint16_t regist) {
  if(_checkReadOnlyVariable(regist) && regInRange(regist)) {
    if(programRunStop == PGM_RUNNING) {
      copySourceRegisterToDestRegister(REGISTER_Y, SAVED_REGISTER_Y);
      copySourceRegisterToDestRegister(REGISTER_X, SAVED_REGISTER_X);
    }

    copySourceRegisterToDestRegister(regist, REGISTER_Y);
    if(getRegisterDataType(REGISTER_Y) == dtShortInteger) {
      *(REGISTER_SHORT_INTEGER_DATA(REGISTER_Y)) &= shortIntegerMask;
    }

    addition[getRegisterDataType(REGISTER_X)][getRegisterDataType(REGISTER_Y)]();

    copySourceRegisterToDestRegister(SAVED_REGISTER_Y, REGISTER_Y);
    _storeValue(regist);
    if(regist != REGISTER_X) {
      copySourceRegisterToDestRegister(SAVED_REGISTER_X, REGISTER_X);
    }

    adjustResult(REGISTER_X, false, true, REGISTER_X, regist, -1);
    uint16_t rows = 1;
    if(regist >= FIRST_NAMED_VARIABLE && isStatsMatrixN(&rows, regist) && regist == findNamedVariable("STATS")) {
      calcSigma(0);
    }
  }
}



void fnStoreSub(uint16_t regist) {
  if(_checkReadOnlyVariable(regist) && regInRange(regist)) {
    if(programRunStop == PGM_RUNNING) {
      copySourceRegisterToDestRegister(REGISTER_Y, SAVED_REGISTER_Y);
      copySourceRegisterToDestRegister(REGISTER_X, SAVED_REGISTER_X);
    }

    copySourceRegisterToDestRegister(regist, REGISTER_Y);
    if(getRegisterDataType(REGISTER_Y) == dtShortInteger) {
      *(REGISTER_SHORT_INTEGER_DATA(REGISTER_Y)) &= shortIntegerMask;
    }

    subtraction[getRegisterDataType(REGISTER_X)][getRegisterDataType(REGISTER_Y)]();

    copySourceRegisterToDestRegister(SAVED_REGISTER_Y, REGISTER_Y);
    _storeValue(regist);
    if(regist != REGISTER_X) {
      copySourceRegisterToDestRegister(SAVED_REGISTER_X, REGISTER_X);
    }

    adjustResult(REGISTER_X, false, true, REGISTER_X, regist, -1);
    uint16_t rows = 1;
    if(regist >= FIRST_NAMED_VARIABLE && isStatsMatrixN(&rows, regist) && regist == findNamedVariable("STATS")) {
      calcSigma(0);
    }
  }
}



void fnStoreMult(uint16_t regist) {
  if(_checkReadOnlyVariable(regist) && regInRange(regist)) {
    if(programRunStop == PGM_RUNNING) {
      copySourceRegisterToDestRegister(REGISTER_Y, SAVED_REGISTER_Y);
      copySourceRegisterToDestRegister(REGISTER_X, SAVED_REGISTER_X);
    }

    copySourceRegisterToDestRegister(regist, REGISTER_Y);
    if(getRegisterDataType(REGISTER_Y) == dtShortInteger) {
      *(REGISTER_SHORT_INTEGER_DATA(REGISTER_Y)) &= shortIntegerMask;
    }

    multiplication[getRegisterDataType(REGISTER_X)][getRegisterDataType(REGISTER_Y)]();

    copySourceRegisterToDestRegister(SAVED_REGISTER_Y, REGISTER_Y);
    _storeValue(regist);
    if(regist != REGISTER_X) {
      copySourceRegisterToDestRegister(SAVED_REGISTER_X, REGISTER_X);
    }

    adjustResult(REGISTER_X, false, true, REGISTER_X, regist, -1);
    uint16_t rows = 1;
    if(regist >= FIRST_NAMED_VARIABLE && isStatsMatrixN(&rows, regist) && regist == findNamedVariable("STATS")) {
      calcSigma(0);
    }
  }
}



void fnStoreDiv(uint16_t regist) {
  if(_checkReadOnlyVariable(regist) && regInRange(regist)) {
    if(programRunStop == PGM_RUNNING) {
      copySourceRegisterToDestRegister(REGISTER_Y, SAVED_REGISTER_Y);
      copySourceRegisterToDestRegister(REGISTER_X, SAVED_REGISTER_X);
    }

    copySourceRegisterToDestRegister(regist, REGISTER_Y);
    if(getRegisterDataType(REGISTER_Y) == dtShortInteger) {
      *(REGISTER_SHORT_INTEGER_DATA(REGISTER_Y)) &= shortIntegerMask;
    }

    division[getRegisterDataType(REGISTER_X)][getRegisterDataType(REGISTER_Y)]();

    copySourceRegisterToDestRegister(SAVED_REGISTER_Y, REGISTER_Y);
    _storeValue(regist);
    if(regist != REGISTER_X) {
      copySourceRegisterToDestRegister(SAVED_REGISTER_X, REGISTER_X);
    }

    adjustResult(REGISTER_X, false, true, REGISTER_X, regist, -1);
    uint16_t rows = 1;
    if(regist >= FIRST_NAMED_VARIABLE && isStatsMatrixN(&rows, regist) && regist == findNamedVariable("STATS")) {
      calcSigma(0);
    }
  }
}



void fnStoreMin(uint16_t regist) {
  if(_checkReadOnlyVariable(regist) && regInRange(regist)) {
    copySourceRegisterToDestRegister(REGISTER_X, SAVED_REGISTER_X);
    if(FIRST_RESERVED_VARIABLE <= regist && regist < LAST_RESERVED_VARIABLE && allReservedVariables[regist - FIRST_RESERVED_VARIABLE].header.pointerToRegisterData == C47_NULL) {
      copySourceRegisterToDestRegister(regist, TEMP_REGISTER_1);
      regist = TEMP_REGISTER_1;
    }
    else if(FIRST_RESERVED_VARIABLE <= regist && regist < LAST_RESERVED_VARIABLE && allReservedVariables[regist - FIRST_RESERVED_VARIABLE].header.pointerToRegisterData == C47_NULL) {
      fnToReal(NOPARAM);
      if(lastErrorCode == ERROR_NONE) {
        copySourceRegisterToDestRegister(regist, TEMP_REGISTER_1);
        regist = TEMP_REGISTER_1;
      }
    }
    registerMin(REGISTER_X, regist, regist);
    copySourceRegisterToDestRegister(SAVED_REGISTER_X, REGISTER_X);
  }
}



void fnStoreMax(uint16_t regist) {
  if(_checkReadOnlyVariable(regist) && regInRange(regist)) {
    copySourceRegisterToDestRegister(REGISTER_X, SAVED_REGISTER_X);
    if(FIRST_RESERVED_VARIABLE <= regist && regist < LAST_RESERVED_VARIABLE && allReservedVariables[regist - FIRST_RESERVED_VARIABLE].header.pointerToRegisterData == C47_NULL) {
      copySourceRegisterToDestRegister(regist, TEMP_REGISTER_1);
      regist = TEMP_REGISTER_1;
    }
    else if(FIRST_RESERVED_VARIABLE <= regist && regist < LAST_RESERVED_VARIABLE && allReservedVariables[regist - FIRST_RESERVED_VARIABLE].header.pointerToRegisterData == C47_NULL) {
      fnToReal(NOPARAM);
      if(lastErrorCode == ERROR_NONE) {
        copySourceRegisterToDestRegister(regist, TEMP_REGISTER_1);
        regist = TEMP_REGISTER_1;
      }
    }
    registerMax(REGISTER_X, regist, regist);
    copySourceRegisterToDestRegister(SAVED_REGISTER_X, REGISTER_X);
  }
}



void fnStoreConfig(uint16_t regist) {
    //uint8_t  compatibility_u8 = 0;             //defaults to use when settings are removed
  bool_t compatibility_bool00 = false;           //defaults to use when settings are removed
  bool_t compatibility_bool0  = false;           //defaults to use when settings are removed
  bool_t compatibility_bool1  = false;           //defaults to use when settings are removed
  bool_t compatibility_bool2  = false;           //defaults to use when settings are removed
  bool_t compatibility_bool3  = false;           //defaults to use when settings are removed
  bool_t compatibility_bool4  = false;           //defaults to use when settings are removed
  bool_t compatibility_bool5  = false;           //defaults to use when settings are removed
  float  compatibility_float1 = 0.1;             //defaults to use when settings are removed
  float  compatibility_float2 = 0.2;             //defaults to use when settings are removed
  float  compatibility_float3 = 0.3;             //defaults to use when settings are removed
  float  compatibility_float4 = 0.4;             //defaults to use when settings are removed
  float  compatibility_float5 = 0.5;             //defaults to use when settings are removed
  float  compatibility_float6 = 0.6;             //defaults to use when settings are removed
  reallocateRegister(regist, dtConfig, CONFIG_SIZE_IN_BLOCKS, amNone);
  dtConfigDescriptor_t *configToStore = REGISTER_CONFIG_DATA(regist);

  storeToDtConfigDescriptor(shortIntegerMode);
  storeToDtConfigDescriptor(shortIntegerWordSize);
  storeToDtConfigDescriptor(displayFormat);
  storeToDtConfigDescriptor(displayFormatDigits);
  storeToDtConfigDescriptor(gapItemLeft);
  storeToDtConfigDescriptor(gapItemRight);
  storeToDtConfigDescriptor(gapItemRadix);
  storeToDtConfigDescriptor(grpGroupingLeft);
  storeToDtConfigDescriptor(grpGroupingGr1LeftOverflow);
  storeToDtConfigDescriptor(grpGroupingGr1Left);
  storeToDtConfigDescriptor(grpGroupingRight);
  storeToDtConfigDescriptor(currentAngularMode);
  storeToDtConfigDescriptor(lrSelection);
  storeToDtConfigDescriptor(lrChosen);
  storeToDtConfigDescriptor(denMax);
  storeToDtConfigDescriptor(displayStack);
  storeToDtConfigDescriptor(firstGregorianDay);
  storeToDtConfigDescriptor(roundingMode);
  storeToDtConfigDescriptor(systemFlags0);
  storeToDtConfigDescriptor(systemFlags1);
  xcopy(configToStore->kbd_usr, kbd_usr, sizeof(kbd_usr));
  storeToDtConfigDescriptor(fgLN);
  storeToDtConfigDescriptor(eRPN);
  storeToDtConfigDescriptor(HOME3);
  storeToDtConfigDescriptor(ShiftTimoutMode);
  storeToDtConfigDescriptor(CPXMULT);
  storeToDtConfigDescriptor(BASE_HOME);
  storeToDtConfigDescriptor(compatibility_bool00);   //added
  storeToDtConfigDescriptor(Norm_Key_00_VAR);
  storeToDtConfigDescriptor(Input_Default);
  storeToDtConfigDescriptor(compatibility_bool0);    //added
  storeToDtConfigDescriptor(BASE_MYM);
  storeToDtConfigDescriptor(jm_G_DOUBLETAP);
  storeToDtConfigDescriptor(compatibility_float1);
  storeToDtConfigDescriptor(compatibility_float2);
  storeToDtConfigDescriptor(compatibility_float3);
  storeToDtConfigDescriptor(compatibility_float4);
  storeToDtConfigDescriptor(compatibility_float5);
  storeToDtConfigDescriptor(compatibility_float6);
  storeToDtConfigDescriptor(compatibility_bool1);
  storeToDtConfigDescriptor(compatibility_bool2);
  storeToDtConfigDescriptor(compatibility_bool3);
  storeToDtConfigDescriptor(PLOT_VECT);
  storeToDtConfigDescriptor(PLOT_NVECT);
  storeToDtConfigDescriptor(PLOT_SCALE);
  storeToDtConfigDescriptor(compatibility_bool4);
  storeToDtConfigDescriptor(PLOT_LINE);
  storeToDtConfigDescriptor(PLOT_CROSS);
  storeToDtConfigDescriptor(PLOT_BOX);
  storeToDtConfigDescriptor(PLOT_INTG);
  storeToDtConfigDescriptor(PLOT_DIFF);
  storeToDtConfigDescriptor(PLOT_RMS);
  storeToDtConfigDescriptor(PLOT_SHADE);
  storeToDtConfigDescriptor(PLOT_AXIS);
  storeToDtConfigDescriptor(PLOT_ZMX);
  storeToDtConfigDescriptor(PLOT_ZMY);
  storeToDtConfigDescriptor(compatibility_bool5);
  storeToDtConfigDescriptor(jm_LARGELI);
  storeToDtConfigDescriptor(constantFractions);
  storeToDtConfigDescriptor(constantFractionsMode);
  storeToDtConfigDescriptor(constantFractionsOn);
  storeToDtConfigDescriptor(displayStackSHOIDISP);
  storeToDtConfigDescriptor(bcdDisplay);
  storeToDtConfigDescriptor(topHex);
  storeToDtConfigDescriptor(bcdDisplaySign);
  storeToDtConfigDescriptor(DRG_Cycling);
  storeToDtConfigDescriptor(DM_Cycling);
  storeToDtConfigDescriptor(SI_All);
  storeToDtConfigDescriptor(LongPressM);
  storeToDtConfigDescriptor(LongPressF);
  storeToDtConfigDescriptor(lastDenominator);
  storeToDtConfigDescriptor(significantDigits);
  storeToDtConfigDescriptor(pcg32_global);
  storeToDtConfigDescriptor(exponentLimit);
  storeToDtConfigDescriptor(exponentHideLimit);
  storeToDtConfigDescriptor(lastIntegerBase);
  storeToDtConfigDescriptor(HOME3);
}



void fnStoreStack(uint16_t regist) {
  uint16_t size = getSystemFlag(FLAG_SSIZE8) ? 8 : 4;

  if(regist + size >= REGISTER_X && regist < REGISTER_X) {
    displayCalcErrorMessage(ERROR_STACK_CLASH, ERR_REGISTER_LINE, REGISTER_X);
    #if(EXTRA_INFO_ON_CALC_ERROR == 1)
      sprintf(errorMessage, "Cannot execute STOS, destination register would overlap the stack: %d", regist);
      moreInfoOnError("In function fnStoreStack:", errorMessage, NULL, NULL);
    #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
  }
  else if((regist >= REGISTER_X && regist < FIRST_LOCAL_REGISTER) || regist + size > FIRST_LOCAL_REGISTER + currentNumberOfLocalRegisters) {
    displayCalcErrorMessage(ERROR_OUT_OF_RANGE, ERR_REGISTER_LINE, REGISTER_X);
    #if(EXTRA_INFO_ON_CALC_ERROR == 1)
      sprintf(errorMessage, "Cannot execute STOS, destination register is out of range: %d", regist);
      moreInfoOnError("In function fnStoreStack:", errorMessage, NULL, NULL);
    #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
  }
  else {
    for(int i=0; i<size; i++) {
      copySourceRegisterToDestRegister(REGISTER_X + i, regist + i);
    }
  }
}


static void _fnStoreElement(bool_t stepForward);


void fnStoreVElement(uint16_t ix) {
  #if !defined(TESTSUITE_BUILD)
  const int16_t iBak = getIRegisterAsInt(true);
  const int16_t jBak = getJRegisterAsInt(true);
  real_t rx;

  if((getRegisterDataType(REGISTER_Y) == dtReal34Matrix) || (getRegisterDataType(REGISTER_Y) == dtComplex34Matrix)) {
    if (!getRegisterAsComplex(REGISTER_X, &rx, &rx) && !getRegisterAsReal(REGISTER_X, &rx)) {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, NIM_REGISTER_LINE);
      #if defined(PC_BUILD)
        sprintf(errorMessage, "DataType %" PRIu32, getRegisterDataType(REGISTER_X));
        moreInfoOnError("In function fnStoreVElement:", errorMessage, "is not a Real/Integer/Complex.", "");
      #endif
      return;
    }
    if(getRegisterDataType(REGISTER_Y) == dtReal34Matrix) {
      real34Matrix_t x;
      linkToRealMatrixRegister(REGISTER_Y, &x);
      setIRegisterAsInt(false, (ix-1) / x.header.matrixColumns+1);
      setJRegisterAsInt(false, (ix-1) % x.header.matrixColumns+1);
    }
    else { //Complex Matrix
      complex34Matrix_t x;
      linkToComplexMatrixRegister(REGISTER_Y, &x);
      setIRegisterAsInt(false, (ix-1) / x.header.matrixColumns+1);
      setJRegisterAsInt(false, (ix-1) % x.header.matrixColumns+1);
    }
    uint16_t matrixIndexBak = matrixIndex;
    matrixIndex = REGISTER_Y;
    _fnStoreElement(false);
    setIRegisterAsInt(false, iBak);
    setJRegisterAsInt(false, jBak);
    matrixIndex = matrixIndexBak;
  }
  else {
    displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, NIM_REGISTER_LINE);
      #if defined(PC_BUILD)
    sprintf(errorMessage, "DataType %" PRIu32, getRegisterDataType(REGISTER_Y));
    moreInfoOnError("In function fnStoreVElement:", errorMessage, "is not a matrix.", "");
    #endif
  }
  #endif // !TESTSUITE_BUILD
}

void fnStoreElementPlus(uint16_t unusedButMandatoryParameter) {
  _fnStoreElement(true);
  temporaryInformation = TI_MIJ;
}

void fnStoreElement(uint16_t unusedButMandatoryParameter) {
  _fnStoreElement(false);
  temporaryInformation = TI_MIJ;
}

void _fnStoreElement(bool_t stepForward) {
  #if !defined(TESTSUITE_BUILD)
    if(matrixIndex == INVALID_VARIABLE) {
      displayCalcErrorMessage(ERROR_NO_MATRIX_INDEXED, ERR_REGISTER_LINE, REGISTER_X);
      #if(EXTRA_INFO_ON_CALC_ERROR == 1)
        sprintf(errorMessage, "Cannot execute STOEL without a matrix indexed");
        moreInfoOnError("In function _fnStoreElement:", errorMessage, NULL, NULL);
      #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
    }
    else {
      if(regInRange(matrixIndex) && getRegisterDataType(matrixIndex) == dtReal34Matrix && getRegisterDataType(REGISTER_X) == dtComplex34) {
        // Real matrices turns to complex matrices by setting a complex element
        convertReal34MatrixRegisterToComplex34MatrixRegister(matrixIndex, matrixIndex);
      }
      callByIndexedMatrix(storeElementReal, storeElementComplex);
      if(stepForward) {
        fnIncDecJ(INC_FLAG);
      }
      uint16_t rows = 1;
      if(matrixIndex >= FIRST_NAMED_VARIABLE && isStatsMatrixN(&rows, matrixIndex) && matrixIndex == findNamedVariable("STATS")) {
        calcSigma(0);
      }
    }
  #endif // !TESTSUITE_BUILD
}



void fnStoreIJ(uint16_t unusedButMandatoryParameter) {
  #if !defined(TESTSUITE_BUILD)
    if(matrixIndex == INVALID_VARIABLE) {
      displayCalcErrorMessage(ERROR_NO_MATRIX_INDEXED, ERR_REGISTER_LINE, REGISTER_X);
      #if(EXTRA_INFO_ON_CALC_ERROR == 1)
        sprintf(errorMessage, "Cannot execute STOIJ without a matrix indexed");
        moreInfoOnError("In function fnStoreIJ:", errorMessage, NULL, NULL);
      #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)
    }
    else {
      callByIndexedMatrix(storeIjReal, storeIjComplex);
      temporaryInformation = TI_IJ;
      uint16_t rows = 1;
      if(matrixIndex >= FIRST_NAMED_VARIABLE && isStatsMatrixN(&rows, matrixIndex) && matrixIndex == findNamedVariable("STATS")) {
        calcSigma(0);
      }
    }
  #endif // !TESTSUITE_BUILD
}
