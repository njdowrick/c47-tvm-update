;*************************************************************
;*************************************************************
;**                                                         **
;**          QF of negative binomial distribution           **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RMODE=0 IM=2compl SS=4 WS=64
Func: fnNegBinomialI



;=======================================
; negBinomialI(Long Integer) --> Real
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=LonI:"5" RX=Real:"0.1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.1" RX=Real:"1"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=LonI:"5" RX=Real:"0.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.5" RX=Real:"4"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=LonI:"5" RX=Real:"0.9"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.9" RX=Real:"9"


In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=LonI:"20" RX=Real:"0.1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.1" RX=Real:"12"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=LonI:"20" RX=Real:"0.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.5" RX=Real:"19"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=LonI:"20" RX=Real:"0.9"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.9" RX=Real:"28"


In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.1" RN=LonI:"5" RX=Real:"0.1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.1" RX=Real:"0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.1" RN=LonI:"5" RX=Real:"0.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.5" RX=Real:"0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.1" RN=LonI:"5" RX=Real:"0.9"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.9" RX=Real:"2"


In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.9" RN=LonI:"5" RX=Real:"0.1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.1" RX=Real:"21"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.9" RN=LonI:"5" RX=Real:"0.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.5" RX=Real:"42"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.9" RN=LonI:"5" RX=Real:"0.9"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.9" RX=Real:"73"



;=======================================
; negBinomialI(Time) --> Error24
;=======================================



;=======================================
; negBinomialI(Date) --> Error24
;=======================================



;=======================================
; negBinomialI(String) --> Error24
;=======================================
In:  FL_ASLIFT=0 RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"



;=======================================
; negBinomialI(Real Matrix) --> Error24
;=======================================



;=======================================
; negBinomialI(Complex Matrix) --> Error24
;=======================================



;=======================================
; negBinomialI(Short Integer) --> Error24
;=======================================



;=======================================
; negBinomialI(Real) --> Real
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=Real:"5" RX=Real:"0.1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.1" RX=Real:"1"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=Real:"5" RX=Real:"0.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.5" RX=Real:"4"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=Real:"5" RX=Real:"0.9"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.9" RX=Real:"9"


In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=Real:"20" RX=Real:"0.1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.1" RX=Real:"12"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=Real:"20" RX=Real:"0.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.5" RX=Real:"19"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.5" RN=Real:"20" RX=Real:"0.9"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.9" RX=Real:"28"


In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.1" RN=Real:"5" RX=Real:"0.1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.1" RX=Real:"0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.1" RN=Real:"5" RX=Real:"0.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.5" RX=Real:"0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.1" RN=Real:"5" RX=Real:"0.9"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.9" RX=Real:"2"


In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.9" RN=Real:"5" RX=Real:"0.1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.1" RX=Real:"21"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.9" RN=Real:"5" RX=Real:"0.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.5" RX=Real:"42"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RP=Real:"0.9" RN=Real:"5" RX=Real:"0.9"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.9" RX=Real:"73"



;=======================================
; negBinomialI(Complex) --> Error24
;=======================================
